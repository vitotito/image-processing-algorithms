import numpy as np
from skimage import io, color

# TODO: extract_pixel_region should extract based on a mask. Instead, the current
# implementation extracts all pixels in a rectangular matrix around the defined
# center, based on the mask size.
def extract_pixel_region(i, j, img_array, mask):
    """
    This function will return the list of pixels that corresponds the region to be evaluated
    """
    width, height = img_array.shape[:2]
    mask_width, mask_height = mask.shape[:2]
    i_step = mask_width // 2
    j_step = mask_height // 2
    min_i = i - i_step if i - i_step > 0 else 0
    max_i = i + i_step if i + i_step < width else width
    min_j = j - j_step if j - j_step > 0 else 0
    max_j = j + j_step if j + j_step < height else height
    region = img_array[min_i : max_i + 1, min_j : max_j + 1]
    return region
    
def get_pixel_value_lab(region, original_pixel):
    """
    This function will return the correct pixel value calculated based on a given region of pixels in LAB format
    """
    pixel = region[:,:,0].max()
    return [pixel, original_pixel[1] , original_pixel[2]]

def get_pixel_value_rgb(region):
    """
    This function will return the correct pixel value calculated based on a given region of pixels in RGB format
    """        
    pixelR = region[:,:,0].max()
    pixelG = region[:,:,1].max()
    pixelB = region[:,:,2].max()
    return [pixelR, pixelG , pixelB]

# Set up images and mask
img_rgb = io.imread("image.jpg")
img_lab = color.rgb2lab(img_rgb)
w, h = img_rgb.shape[0], img_rgb.shape[1]


mask = np.array([
    [0, 1, 0],
    [1, 1, 1],
    [0, 1, 0]
])
# TODO: The above mask is not supported, so we just define a 15x15 matrix of ones for simplicity
mask = np.ones((15, 15))

data_lab = np.zeros((w, h, 3), dtype=np.float64)
data_rgb = np.zeros((w, h, 3), dtype=np.float64)

# Loop through the matrix and calculate the pixel values per element

for i in range(w):
    for j in range(h):
        region_lab = extract_pixel_region(i, j, img_lab, mask)
        data_lab[i,j] = get_pixel_value_lab(region_lab, img_lab[i,j])
        region_rgb = extract_pixel_region(i, j, img_rgb, mask)
        data_rgb[i,j] = get_pixel_value_rgb(region_rgb)

# TODO: These imsave calls give a warning about a lossy conversion. Investigate
io.imsave('result_lab.png', color.lab2rgb(data_lab))
io.imsave('result_rgb.png', data_rgb)